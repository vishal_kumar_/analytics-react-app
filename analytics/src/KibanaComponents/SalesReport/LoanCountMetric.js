import React, { Component } from 'react';


class LoanCountMetric extends Component {

    render(){
        return(
            <div className='iframe-div'>
              <iframe 
                src="https://analysis.gromor.in/app/kibana#/visualize/edit/9efa20d0-4491-11e9-839e-7d9427db5fa5?embed=true&_g=()&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'2',params:(customLabel:'Loan+Count'),schema:metric,type:count)),params:(addLegend:!f,addTooltip:!t,metric:(colorSchema:'Green+to+Red',colorsRange:!((from:0,to:10000)),invertColors:!f,labels:(show:!t),metricColorMode:None,percentageMode:!f,style:(bgColor:!f,bgFill:%23000,fontSize:30,labelColor:!f,subText:''),useRanges:!f),type:metric),title:'LOAN+COUNT+METRICS',type:metric))" 
                height="600" 
                width="800">
              </iframe>
            </div>
        );
    }
}
export default LoanCountMetric;