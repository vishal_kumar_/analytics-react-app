import React, { Component } from 'react';
class SalesManagerData extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/9ea60e20-6000-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:lucene,query:''),uiState:(vis:(params:(sort:(columnIndex:!n,direction:!n)))),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Loan+Amount',field:outflow_amount),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Average+Ticket+Size',field:outflow_amount),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'3',params:(customLabel:Count,field:id),schema:metric,type:cardinality),(enabled:!t,hidden:!f,id:'4',params:(customLabel:'Sales+Manager',field:lead_sales_assignee.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:40),schema:bucket,type:terms),(enabled:!t,hidden:!f,id:'5',params:(customLabel:'Average+Tenure',field:proposal_tenure),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'6',params:(customLabel:'Average+Rate+Of+Interest',field:rate_of_interest),schema:metric,type:avg)),params:(perPage:!n,showMetricsAtAllLevels:!f,showPartialRows:!f,showTotal:!f,sort:(columnIndex:!n,direction:!n),totalFunc:sum),title:'LOAN+DATA+ACROSS+SALES+MANAGER+DATA+TABLE+V2',type:table))" 
                height="600" 
                width="1000">
             </iframe>
            </div>
        );
    }
}
export default SalesManagerData;