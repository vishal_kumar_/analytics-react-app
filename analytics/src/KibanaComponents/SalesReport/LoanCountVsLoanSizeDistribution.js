import React, { Component } from 'react';
class LoanCountVsLoanSizeDistribution extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/0e086650-44bd-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Loan+Count'),schema:metric,type:count),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Sales+Manager',field:lead_sales_assignee.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:5),schema:segment,type:terms),(enabled:!t,hidden:!f,id:'3',params:(customLabel:'Loan+Size',field:loan_disbursed,ranges:!((from:0,to:200000),(from:200001,to:500000),(from:500001,to:700000),(from:700000))),schema:group,type:range)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:100),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,seriesParams:!((data:(id:'1',label:'Loan+Count'),drawLinesBetweenPoints:!t,mode:stacked,show:true,showCircles:!t,type:histogram,valueAxis:ValueAxis-1)),times:!(),type:histogram,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Loan+Count'),type:value))),title:'LOAN+COUNT+VS+LOAN+SIZE+DISTRIBUTION+FOR+SALES',type:histogram))" 
             height="600" 
             width="800">
             </iframe>
            </div>
        );
    }
}
export default LoanCountVsLoanSizeDistribution;