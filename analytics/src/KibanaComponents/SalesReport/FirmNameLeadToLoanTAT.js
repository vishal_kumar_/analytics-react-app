import React, { Component } from 'react';
class FirmNameLeadToLoanTaT extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/7d1fb600-4a9a-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'TAT+Lead+To+Loan+(In+Days)',field:lead_to_loan),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Firm+Name',field:firm_name.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:20),schema:segment,type:terms)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:30),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,seriesParams:!((data:(id:'1',label:'TAT+Lead+To+Loan+(In+Days)'),drawLinesBetweenPoints:!t,mode:stacked,show:true,showCircles:!t,type:histogram,valueAxis:ValueAxis-1)),times:!(),type:histogram,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'TAT+Lead+To+Loan+(In+Days)'),type:value))),title:'FIRM+NAME+(LOAN+CONVERTED)+',type:histogram))" 
                height="600" 
                width="800">
             </iframe>
            </div>
        );
    }
}
export default FirmNameLeadToLoanTaT;