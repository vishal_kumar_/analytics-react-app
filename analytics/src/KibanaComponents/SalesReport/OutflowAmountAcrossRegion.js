import React, { Component } from 'react';
class OutflowAcrossRegion extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/3cd7c520-5773-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'2b4efc10-5769-11e9-839e-7d9427db5fa5',key:lead_sales_assignee_role.keyword,negate:!f,params:(query:SALESOFFICER,type:phrase),type:phrase,value:SALESOFFICER),query:(match:(lead_sales_assignee_role.keyword:(query:SALESOFFICER,type:phrase))))),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(field:outflow_amount),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Sales+Manager',field:lead_sales_assignee_region.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:5),schema:segment,type:terms)),params:(addLegend:!t,addTooltip:!t,isDonut:!f,labels:(last_level:!f,show:!t,truncate:100,values:!t),legendPosition:right,type:pie),title:'OUTFLOW+LOAN+AMOUNT+ACROSS+REGIONS',type:pie))" 
             height="600" 
             width="800">
             </iframe>
            </div>
        );
    }
}
export default OutflowAcrossRegion;