import React, { Component } from 'react';
class LoanCountLastMonth extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/89744e60-6003-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'loan-converted-outflow*',key:outflow_date,negate:!f,params:(gte:now-1M%2FM,lt:now-0M%2FM),type:range,value:'now-1M%2FM+to+now-0M%2FM'),range:(outflow_date:(gte:now-1M%2FM,lt:now-0M%2FM))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'loan-converted-outflow*',key:lead_sales_assignee_role.keyword,negate:!f,params:(query:SALESOFFICER,type:phrase),type:phrase,value:SALESOFFICER),query:(match:(lead_sales_assignee_role.keyword:(query:SALESOFFICER,type:phrase))))),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Loan+Count',field:id),schema:metric,type:cardinality)),params:(addLegend:!f,addTooltip:!t,metric:(colorSchema:'Green+to+Red',colorsRange:!((from:0,to:10000)),invertColors:!f,labels:(show:!t),metricColorMode:None,percentageMode:!f,style:(bgColor:!f,bgFill:%23000,fontSize:30,labelColor:!f,subText:''),useRanges:!f),type:metric),title:'LOAN+COUNT+FOR+LAST+MONTH+METRICS',type:metric))"
                height="600"
                width="800">
             </iframe>
            </div>
        );
    }
}
export default LoanCountLastMonth;