import React, { Component } from 'react';
class LoanOutflowValueAndCountMoM extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe 
                src="https://analysis.gromor.in/app/kibana#/visualize/edit/5b46d340-5771-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(vis:(params:(sort:(columnIndex:!n,direction:!n)))),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Amount+Disbursed',field:outflow_amount),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customInterval:'2h',customLabel:Months,drop_partials:!f,extended_bounds:(),field:outflow_date,interval:M,min_doc_count:1,timeRange:(from:'2019-02-28T18:30:00.000Z',mode:absolute,to:'2019-03-31T18:29:59.999Z'),time_zone:Asia%2FKolkata,useNormalizedEsInterval:!t),schema:bucket,type:date_histogram),(enabled:!t,hidden:!f,id:'3',params:(customLabel:Count,field:id),schema:metric,type:cardinality)),params:(perPage:10,showMetricsAtAllLevels:!f,showPartialRows:!f,showTotal:!f,sort:(columnIndex:!n,direction:!n),totalFunc:sum),title:'MOM+OUTFLOW+LOAN+VALUE+AND+COUNT+TABLE',type:table))"
                height="600"
                width="800">
             </iframe>
            </div>
        );
    }
}
export default LoanOutflowValueAndCountMoM;