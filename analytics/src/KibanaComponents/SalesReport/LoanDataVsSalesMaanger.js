import React, { Component } from 'react';
class LoanDataVsSalesManagerChart extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/2d0e5b20-5770-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Total+Loan+Disbursed',field:loan_disbursed),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Sales+Manager',field:lead_sales_assignee.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:40),schema:segment,type:terms),(enabled:!t,hidden:!f,id:'3',params:(customLabel:'Average+Loan+Amount+Disbursed',field:loan_disbursed),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'4',params:(customLabel:'Loan+Count'),schema:metric,type:count)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:100),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,seriesParams:!((data:(id:'1',label:'Total+Loan+Disbursed'),drawLinesBetweenPoints:!t,mode:stacked,show:true,showCircles:!t,type:histogram,valueAxis:ValueAxis-1),(data:(id:'3',label:'Average+Loan+Amount+Disbursed'),drawLinesBetweenPoints:!t,mode:normal,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-2),(data:(id:'4',label:'Loan+Count'),drawLinesBetweenPoints:!t,mode:normal,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-3)),times:!(),type:histogram,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Total+Loan+Disbursed'),type:value),(id:ValueAxis-2,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-1,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Average+Loan+Amount+Disbursed'),type:value),(id:ValueAxis-3,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-2,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Loan+Count'),type:value))),title:'LOAN+DATA+VS+SALES+MANAGER+V2',type:histogram))" 
                height="600" 
                width="1200">
             </iframe>
            </div>
        );
    }
}
export default LoanDataVsSalesManagerChart;