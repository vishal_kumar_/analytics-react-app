import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem
 } from 'reactstrap';

 import {Link} from 'react-router-dom';


class SideNavPage extends Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false,
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      render() {
        return (
            <div>
              <Navbar color="light" light expand="md">
              <NavbarBrand href="/">Finezza</NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto">
                <NavItem>
                <Link to="/sales" className="link">Sales Report</Link>
              </NavItem>
              <NavItem>
                <Link to="/collections" className="link">Collection Report</Link>
              </NavItem>
              </Nav>
          </Collapse>
        </Navbar>
            </div>
        );
      }
}

export default SideNavPage;