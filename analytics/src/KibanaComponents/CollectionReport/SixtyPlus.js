import React, { Component } from 'react';
class SixtyPlus extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/6ed77e40-c52c-11e9-a285-c7392ff21ba6?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(('$state':(store:appState),exists:(field:write_off_date),meta:(alias:!n,disabled:!f,index:e974c200-4a90-11e9-839e-7d9427db5fa5,key:write_off_date,negate:!t,type:exists,value:exists))),linked:!f,query:(language:lucene,query:''),uiState:(vis:(defaultColors:('0+-+0.05':'rgb(0,104,55)','0.05+-+0.2':'rgb(165,0,38)'))),vis:(aggs:!((enabled:!t,hidden:!t,id:'1',params:(field:POS),schema:metric,type:sum),(enabled:!t,hidden:!t,id:'2',params:(field:sixty_plus_pos),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'3',params:(customLabel:'60%2B+DPD+:+PAR',formatter:percent,formula:agg2%2Fagg1),schema:metric,type:datasweet_formula)),params:(addLegend:!t,addTooltip:!t,gauge:(backStyle:Full,colorSchema:'Green+to+Red',colorsRange:!((from:0,to:0.05),(from:0.05,to:0.2)),extendRange:!t,gaugeColorMode:Labels,gaugeStyle:Full,gaugeType:Arc,invertColors:!f,labels:(color:black,show:!t),orientation:vertical,percentageMode:!f,scale:(color:%23333,labels:!f,show:!t),style:(bgColor:!f,bgFill:%23eee,bgMask:!f,bgWidth:0.9,fontSize:60,labelColor:!t,mask:!f,maskBars:50,subText:'Scale+is+0%25-20%25',width:0.9),type:meter,verticalSplit:!f),isDisplayWarning:!f,type:gauge),title:'60%2B+DPD+:+PAR',type:gauge))" 
             height="600" 
             width="800">
             </iframe>
            </div>
        );
    }
}
export default SixtyPlus;