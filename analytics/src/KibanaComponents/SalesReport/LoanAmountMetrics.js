import React, { Component } from 'react';


class LoanAmount extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/92a62720-4491-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(),vis:(aggs:!((enabled:!t,hidden:!f,id:'2',params:(customLabel:'Total+Proposed+Loan+Amount',field:proposed_amount),schema:metric,type:sum)),params:(addLegend:!f,addTooltip:!t,metric:(colorSchema:'Green+to+Red',colorsRange:!((from:0,to:10000)),invertColors:!f,labels:(show:!t),metricColorMode:None,percentageMode:!f,style:(bgColor:!f,bgFill:%23000,fontSize:30,labelColor:!f,subText:''),useRanges:!f),type:metric),title:'LOAN+AMOUNT+METRICS',type:metric))" 
               height="600"
               width="800">
              </iframe>
            </div>
        );
    }
}
export default LoanAmount;