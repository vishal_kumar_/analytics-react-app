import React from 'react';
import './App.css';
import SideNav from './component/SideNavPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import SalesReport from './component/SalesReport'
import CollectionReport from './component/CollectionReport';
import LoanCount from './KibanaComponents/SalesReport/LoanCountMetric';
import LoanAmount from './KibanaComponents/SalesReport/LoanAmountMetrics';
import LoanOutflowAmount from './KibanaComponents/SalesReport/LoanOutflowAmount';
import LoanAverage from './KibanaComponents/SalesReport/LoanAverageMetrics';
import LoanDataVsSalesManager from './KibanaComponents/SalesReport/LoanDataVsSalesMaanger';
import SalesManagerData from './KibanaComponents/SalesReport/SalesManagerData';
import SalesManagerDataLastMonth from './KibanaComponents/SalesReport/SalesManagerDataLastMonth';
import SalesManagerLoanDataLastMonth from './KibanaComponents/SalesReport/SalesManagerLoanDataLastMonth';
import LoanCountLastMonth from './KibanaComponents/SalesReport/LoanCountLastMonth';
import LoanOutflowAverageLastMonth from './KibanaComponents/SalesReport/LoanOutflowAverageLastMonth';
import LoanOutflowAmountLastMonth from './KibanaComponents/SalesReport/LoanOutflowAmountLastMonth';
import LoanValueAndCountMoM from './KibanaComponents/SalesReport/LoanValueAndCountMoM';
import LoanOutflowValueAndCountMoM from './KibanaComponents/SalesReport/LoanOutflowValueAndCountMoM';
import LoanOutflowAverageValueAndCountMoM from './KibanaComponents/SalesReport/LoanOutflowAverageValueAndCountMoM';
import LoanValueAndCountVsCity from './KibanaComponents/SalesReport/LoanValueAndCountVsCity';
import LoanCountVsLoanSizeDistribution from './KibanaComponents/SalesReport/LoanCountVsLoanSizeDistribution';
import FirmTaT from './KibanaComponents/SalesReport/FirmNameLeadToLoanTAT';
import OutflowAcrossRegion from './KibanaComponents/SalesReport/OutflowAmountAcrossRegion';
import OutflowAmountVsRegion from './KibanaComponents/SalesReport/OutflowAmountVsRegion';
import LoanDataAcrossRegionData from './KibanaComponents/SalesReport/LoanDataAcrossRegionData';
import LoanDataAcrossRegionDataLastMonth from './KibanaComponents/SalesReport/LoanDataAcrossRegionDataLastMonth';
import ZeroPlus from './KibanaComponents/CollectionReport/ZeroPlus';
import ThirtyPlus from './KibanaComponents/CollectionReport/ThirtyPlus';
import SixtyPlus from './KibanaComponents/CollectionReport/SixtyPlus';
import NintyPlus from './KibanaComponents/CollectionReport/NintyPlus';
import ThirtyPlusLaggedDelinquency from './KibanaComponents/CollectionReport/ThirtyPlusLaggedDelinquency';
import SixtyPlusLaggedDelinquency from './KibanaComponents/CollectionReport/SixtyPlusLaggedDelinquency';
import NintyPlusLaggedDelinquency from './KibanaComponents/CollectionReport/NintyPlusLaggedDelinquency';
import EMIOverdue from './KibanaComponents/CollectionReport/EMIOverdue';
import { BrowserRouter as Router, Route } from "react-router-dom";



function App() {
  return (
    <div className="App">
      <Router>
        <Route  path="/"  component={SideNav} />
        <Route exact path="/sideNav"  component={SideNav} />
        <Route exact path="/sales"  component={SalesReport} />
        <Route exact path="/collections"  component={CollectionReport} />
        <Route exact path="/sales/loanCount" component={LoanCount}/>
        <Route exact path="/sales/loanAmount" component={LoanAmount}/>
        <Route exact path="/sales/loanOutflowAmount" component={LoanOutflowAmount}/>
        <Route exact path="/sales/loanAverage" component={LoanAverage}/>
        <Route exact path="/sales/loanVsSales" component={LoanDataVsSalesManager}/>
        <Route exact path="/sales/salesManagerData" component={SalesManagerData}/>
        <Route exact path="/sales/salesManagerDataLastMonth" component={SalesManagerDataLastMonth}/>
        <Route exact path="/sales/salesManagerLoanDataLastMonth" component={SalesManagerLoanDataLastMonth}/>
        <Route exact path="/sales/loanCountLastMonth" component={LoanCountLastMonth}/>
        <Route exact path="/sales/loanOutflowAverageLastMonth" component={LoanOutflowAverageLastMonth}/>
        <Route exact path="/sales/loanOutflowAmountLastMonth" component={LoanOutflowAmountLastMonth}/>
        <Route exact path="/sales/loanValueAndCountMoM" component={LoanValueAndCountMoM}/>
        <Route exact path="/sales/loanOutflowValueAndCountMoM" component={LoanOutflowValueAndCountMoM}/>
        <Route exact path="/sales/loanOutflowAvergateValueAndCountMoM" component={LoanOutflowAverageValueAndCountMoM}/>
        <Route exact path="/sales/loanValueAndCountVsCity" component={LoanValueAndCountVsCity}/>
        <Route exact path="/sales/countVsSizeDistribution" component={LoanCountVsLoanSizeDistribution}/>
        <Route exact path="/sales/firmTaT" component={FirmTaT}/>
        <Route exact path="/sales/outflowAcrossRegion" component={OutflowAcrossRegion}/>
        <Route exact path="/sales/outflowAmountVsRegion" component={OutflowAmountVsRegion}/>
        <Route exact path="/sales/loanAcrossRegion" component={LoanDataAcrossRegionData}/>
        <Route exact path="/sales/loanAcrossRegionLastMonth" component={LoanDataAcrossRegionDataLastMonth}/>
        <Route exact path="/collection/zeroPlus" component={ZeroPlus}/>
        <Route exact path="/collection/thirtyPlus" component={ThirtyPlus}/>
        <Route exact path="/collection/sixtyPlus" component={SixtyPlus}/>
        <Route exact path="/collection/nintyPlus" component={NintyPlus}/>
        <Route exact path="/collection/thirtyPlusLagged" component={ThirtyPlusLaggedDelinquency}/>
        <Route exact path="/collection/sixtyPlusLagged" component={SixtyPlusLaggedDelinquency}/>
        <Route exact path="/collection/nintyPlusLagged" component={NintyPlusLaggedDelinquency}/>
        <Route exact path="/collection/overdue" component={EMIOverdue}/>
      </Router>
    </div>
  );
}

export default App;
