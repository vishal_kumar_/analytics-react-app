import React, {Component} from 'react';
import {Container} from 'reactstrap';

import ReactSpeedometer from "react-d3-speedometer";
import LineChart from './LineChart';

const gaugeRow={
    display:'flex',
    justifyContent:'space-around',
    paddingTop:'20px'
}

class CollectionReport extends Component {
    constructor(props){
        super(props);
        this.state={
            
        }
    }
    

    render(){
        return(
            <Container>
                 <div style={gaugeRow}>    
                  <ReactSpeedometer
                    width={250}
                    ringWidth={20}
                    needleHeightRatio={0.7}
                    maxSegmentLabels={6}
                    segments={10}
                    maxValue={20}
                    value={3.92}
                    startColor='green'
                    endColor='red'
                    currentValueText="0+ DPD PAR:  ${value}%"
                  />
                  <ReactSpeedometer
                    width={250}
                    ringWidth={20}
                    needleHeightRatio={0.7}
                    maxSegmentLabels={6}
                    segments={10}
                    maxValue={20}
                    value={0.51}
                    startColor='green'
                    endColor='red'
                    currentValueText="30+ DPD PAR:  ${value}%"
                  />
                  <ReactSpeedometer
                    width={250}
                    ringWidth={20}
                    needleHeightRatio={0.7}
                    maxSegmentLabels={6}
                    segments={10}
                    maxValue={20}
                    value={0.27}
                    startColor='green'
                    endColor='red'
                    currentValueText="60+ DPD PAR:  ${value}%"
                  />
                  <ReactSpeedometer
                    width={250}
                    ringWidth={20}
                    needleHeightRatio={0.7}
                    maxSegmentLabels={6}
                    segments={10}
                    maxValue={20}
                    value={0.49}
                    startColor='green'
                    endColor='red'
                    currentValueText="90+ DPD PAR:  ${value}%"
                  />
                </div> 
            <LineChart/>
            </Container>
        );
    }
}
export default CollectionReport;