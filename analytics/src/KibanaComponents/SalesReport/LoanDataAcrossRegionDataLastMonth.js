import React, { Component } from 'react';
class LoanDataAcrossRegionDataLastMonth extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/ba55b8b0-5fff-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:ded96d50-5fa3-11e9-839e-7d9427db5fa5,key:lead_sales_assignee_role.keyword,negate:!f,params:(query:SALESOFFICER,type:phrase),type:phrase,value:SALESOFFICER),query:(match:(lead_sales_assignee_role.keyword:(query:SALESOFFICER,type:phrase)))),('$state':(store:appState),meta:(alias:!n,disabled:!f,index:ded96d50-5fa3-11e9-839e-7d9427db5fa5,key:outflow_date,negate:!f,params:(gte:now-1M%2FM,lt:now-1M%2Fd),type:range,value:'now-1M%2FM+to+now-1M%2Fd'),range:(outflow_date:(gte:now-1M%2FM,lt:now-1M%2Fd)))),linked:!f,query:(language:lucene,query:''),uiState:(vis:(params:(sort:(columnIndex:!n,direction:!n)))),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Loan+Amount',field:outflow_amount),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customLabel:'Average+Ticket+Size',field:outflow_amount),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'3',params:(customLabel:Count,field:id),schema:metric,type:cardinality),(enabled:!t,hidden:!f,id:'4',params:(customLabel:Region,field:lead_sales_assignee_region.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:5),schema:bucket,type:terms),(enabled:!t,hidden:!f,id:'5',params:(customLabel:'Average+Tenure',field:proposal_tenure),schema:metric,type:avg),(enabled:!t,hidden:!f,id:'6',params:(customLabel:'Average+Rate+Of+Interest',field:rate_of_interest),schema:metric,type:avg)),params:(perPage:5,showMetricsAtAllLevels:!f,showPartialRows:!f,showTotal:!f,sort:(columnIndex:!n,direction:!n),totalFunc:sum),title:'LOAN+DATA+OF+LAST+MONTH+TILL+DAY+ACROSS+REGION+DATA+TABLE+V2',type:table))" 
             height="600" 
             width="800">
             </iframe>
            </div>
        );
    }
}
export default LoanDataAcrossRegionDataLastMonth;