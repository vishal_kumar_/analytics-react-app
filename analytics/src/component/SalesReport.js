import React, { Component } from 'react';
import Highcharts from 'highcharts';
import {
    HighchartsChart, Chart, withHighcharts, XAxis, YAxis, Title, Legend, ColumnSeries, SplineSeries, PieSeries
  } from 'react-jsx-highcharts';
import {Container,Card, CardText, CardBody, CardTitle, CardSubtitle,Table} from 'reactstrap';

const pieData = [{
  name: 'Jane',
  y: 13
}, {
  name: 'John',
  y: 23
}, {
  name: 'Joe',
  y: 19
}];
var tooltip = {
  valueSuffix: '\xB0C'
}
const marginTop = {
  marginTop:'50px'
}

class SalesReport extends Component {
    render(){
        return(
            <Container>
              <div className="card-container">
                <div>
                    <Card className="card">
                        <CardBody>
                        <CardTitle>LOAN COUNT METRICS</CardTitle>
                        <CardText>321</CardText>
                        <CardSubtitle>Loan Count</CardSubtitle>
                        </CardBody>
                    </Card>
                </div>
                <div>
                    <Card className="card">
                        <CardBody>
                        <CardTitle>LOAN AMOUNT METRICS</CardTitle>
                        <CardText>139,310,960</CardText>
                        <CardSubtitle>Total Proposed Loan Amount</CardSubtitle>
                        </CardBody>
                    </Card>
                </div>
              </div>
              <HighchartsChart tooltip={tooltip}>
                <Chart />
                  <Title>Combination chart</Title>
                   <Legend />
                    <XAxis categories={['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']} />
                    <YAxis>
                      <ColumnSeries name="Jane" data={[3, 2, 1, 3, 4]} />
                      <ColumnSeries name="John" data={[2, 3, 5, 7, 6]} />
                      <ColumnSeries name="Joe" data={[4, 3, 3, 9, 0]} />
                      <SplineSeries name="Average" data={[3, 2.67, 3, 6.33, 3.33]} />
                      <PieSeries name="Total consumption" data={pieData} center={[100, 80]} size={100} showInLegend={false} />
                    </YAxis>
                </HighchartsChart>

                <Table style={marginTop}>
                    <thead>
                      <tr>
                        <th>Sales Manager</th>
                        <th>Loan Amount</th>
                        <th>Average Ticket Size</th>
                        <th>Count</th>
                        <th>Average Tenure</th>
                        <th>Average Rate Of Interest</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Abhijeet Kulkarni</td>
                        <td>3,100,000</td>
                        <td>310,000</td>
                        <td>9</td>
                        <td>23.7</td>
                        <td>23.5%</td>
                      </tr>
                      <tr>
                        <td>Amit Malviya</td>
                        <td>400,000	</td>
                        <td>400,000	</td>
                        <td>1</td>
                        <td>24</td>
                        <td>23.5%</td>
                      </tr>
                      <tr>
                        <td>Digvijay Yadav</td>
                        <td>7,950,000	</td>
                        <td>530,000	</td>
                        <td>15</td>
                        <td>25.2</td>
                        <td>23.13%</td>
                      </tr>
                      <tr>
                        <td>Moshin Mulani</td>
                        <td>7,337,555</td>
                        <td>407,642</td>
                        <td>18</td>
                        <td>24.83</td>
                        <td>23.14%</td>
                      </tr>
                    </tbody>
                  </Table>
            </Container>
        );
    }
}
export default withHighcharts(SalesReport, Highcharts);