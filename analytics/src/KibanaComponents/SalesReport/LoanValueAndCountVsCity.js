import React, { Component } from 'react';
class LoanValueAndCountVsCity extends Component {

    render(){
        return(
            <div className='iframe-div'>
             <iframe src="https://analysis.gromor.in/app/kibana#/visualize/edit/1780c940-44bb-11e9-839e-7d9427db5fa5?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-6M,mode:quick,to:now))&_a=(filters:!(),linked:!f,query:(language:kuery,query:''),uiState:(vis:(colors:('Average+Loan+Amount':%231F78C1,'Loan+Count':%23E24D42))),vis:(aggs:!((enabled:!t,hidden:!f,id:'1',params:(customLabel:'Total+Loan+Amount',field:loan_disbursed),schema:metric,type:sum),(enabled:!t,hidden:!f,id:'2',params:(customLabel:City,field:firm_city.keyword,missingBucket:!f,missingBucketLabel:Missing,order:desc,orderBy:'1',otherBucket:!f,otherBucketLabel:Other,size:20),schema:segment,type:terms),(enabled:!t,hidden:!f,id:'3',params:(customLabel:'Loan+Count'),schema:metric,type:count),(enabled:!t,hidden:!f,id:'4',params:(customLabel:'Average+Loan+Amount',field:loan_disbursed),schema:metric,type:avg)),params:(addLegend:!t,addTimeMarker:!f,addTooltip:!t,categoryAxes:!((id:CategoryAxis-1,labels:(show:!t,truncate:100),position:bottom,scale:(type:linear),show:!t,style:(),title:(),type:category)),grid:(categoryLines:!f,style:(color:%23eee)),legendPosition:right,seriesParams:!((data:(id:'1',label:'Total+Loan+Amount'),drawLinesBetweenPoints:!t,mode:stacked,show:true,showCircles:!t,type:histogram,valueAxis:ValueAxis-1),(data:(id:'3',label:'Loan+Count'),drawLinesBetweenPoints:!t,mode:normal,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-2),(data:(id:'4',label:'Average+Loan+Amount'),drawLinesBetweenPoints:!t,mode:normal,show:!t,showCircles:!t,type:line,valueAxis:ValueAxis-3)),times:!(),type:histogram,valueAxes:!((id:ValueAxis-1,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:LeftAxis-1,position:left,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Total+Loan+Amount'),type:value),(id:ValueAxis-2,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-1,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Loan+Count'),type:value),(id:ValueAxis-3,labels:(filter:!f,rotate:0,show:!t,truncate:100),name:RightAxis-2,position:right,scale:(mode:normal,type:linear),show:!t,style:(),title:(text:'Average+Loan+Amount'),type:value))),title:'LOAN+VALUE+AND+COUNT+VS+CITY+DISTRIBUTION',type:histogram))"
             height="600"
             width="800">
             </iframe>
            </div>
        );
    }
}
export default LoanValueAndCountVsCity;